//
//  ViewController.swift
//  ARTest
//
//  Created by test on 11.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

enum Objects: Int{
    case box, ship, car, aircraft
}

class ARViewController: UIViewController{

    @IBOutlet var sceneView: ARSCNView!
    let configuration = ARWorldTrackingConfiguration()
    
    var scale: CGFloat = 1.0
    var nodeNumber = 1
    
    
    var shipNode: SCNNode!
    var boxNode: SCNNode!
    var carNode: SCNNode!
    var aircraftNode: SCNNode!
    
    var selectedNode: SCNNode!
    var planeAnchor: ARPlaneAnchor!
    
    let shipScene = SCNScene(named: "art.scnassets/veliero/veliero.dae")!
    let carScene = SCNScene(named: "art.scnassets/car/car.dae")!
    let aircraftScene = SCNScene(named: "art.scnassets/aircraft/aircraft.scn")!
    
    
    var color: UIColor{
        return #colorLiteral(red: 0.4941176471, green: 0.8274509804, blue: 0.1294117647, alpha: 0.7)
    }
    
    var button: UIButton{
        let button = UIButton(frame: CGRect(x: 10, y: 10, width: 50, height: 50))
        button.setBackgroundImage(#imageLiteral(resourceName: "ic_add"), for: .normal)
        button.tintColor = color
        button.setTitle("", for: .normal)
        button.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        return button
    }

    var state: UIControlState = .normal
    
    var layout: UICollectionViewFlowLayout{
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: view.bounds.width / 3, height: 100)
        layout.minimumInteritemSpacing = 0.0
        layout.minimumLineSpacing = 0.0
        layout.scrollDirection = .horizontal
        return layout
    }
    var collectionView: UICollectionView!
        
    var items = ["icon_box", "icon_ship", "icon_car", "icon_aircraft"]
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shipNode = shipScene.rootNode.childNode(withName: "veliero", recursively: true)!
        carNode = carScene.rootNode.childNode(withName: "car", recursively: true)!
        boxNode = SCNNode()
        aircraftNode = aircraftScene.rootNode.childNode(withName: "aircraft", recursively: true)!
        configUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupSceneView()
    }
    
    func setupSceneView(){
        configuration.planeDetection = .horizontal
        
        sceneView.session.run(configuration)
        
        sceneView.delegate = self
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
//        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
    }
    
    func configUI(){
        view.addSubview(button)

        collectionView = UICollectionView(frame: CGRect(x: 0, y: view.bounds.height, width: view.bounds.width, height: 100), collectionViewLayout: layout)
        collectionView.backgroundColor = color
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(Cell.nib, forCellWithReuseIdentifier: Cell.reuseId)
        collectionView.delegate = self
        collectionView.dataSource = self
        view.addSubview(collectionView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    func addGesture(){
        let directions: [UISwipeGestureRecognizerDirection] = [.left, .right]
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(didRecognize(_:)))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didRecognize(_:)))
        for direction in directions{
            let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(didRecognize(_:)))
            swipeGesture.direction = direction
            DispatchQueue.main.async {[weak self] in
                guard let strongSelf = self else { return }
                tapGesture.cancelsTouchesInView = false
                pinchGesture.cancelsTouchesInView = false
                swipeGesture.cancelsTouchesInView = false
                strongSelf.sceneView.addGestureRecognizer(swipeGesture)
                strongSelf.sceneView.addGestureRecognizer(pinchGesture)
                strongSelf.sceneView.addGestureRecognizer(tapGesture)
            }
        } 
    }
    
    @objc func didRecognize(_ sender: UIGestureRecognizer){
        if let rotation = sender as? UISwipeGestureRecognizer{
            if rotation.state == .ended{
                let node = getNode(from: rotation)
                handleRotation(for: node, direction: rotation.direction)
            }
        }else if let pinch = sender as? UIPinchGestureRecognizer{
            if pinch.state == .changed{
                let node = getNode(from: pinch)
                scale = pinch.scale
                handlePinch(for: node, scale: scale)
                pinch.scale = 1.0
            }
        }else if let tap = sender as? UITapGestureRecognizer{
            let node = getNode(from: tap)
            guard let name = node.name else { return }
            removeNode(named: name)
        }
    }
    
    @objc func buttonAction(_ sender: UIButton){
        switch state{
        case .normal:
            UIView.animate(withDuration: 0.2) {[weak self] in
                guard let strongSelf = self else { return }
                sender.transform = CGAffineTransform.identity.rotated(by: 40.0)
                strongSelf.state = .highlighted
                strongSelf.collectionView.frame.origin.y -= strongSelf.collectionView.frame.height
            }
        case .highlighted:
            UIView.animate(withDuration: 0.2) {[weak self] in
                guard let strongSelf = self else { return }
                sender.transform = CGAffineTransform.identity.rotated(by: 0.0)
                strongSelf.state = .normal
                strongSelf.collectionView.frame.origin.y = strongSelf.view.bounds.height
            }
        default:
            break
        }
    }
    
    func getNode(from gesture: UIGestureRecognizer) -> SCNNode{
//        if guards fails
        let fakeNode = SCNNode()
        
        let location = gesture.location(in: sceneView)
        let hits = sceneView.hitTest(location, options: nil)
        guard !hits.isEmpty else { return fakeNode }
        guard let node = hits.first?.node else { return fakeNode }
        return node
    }
    
    func handleRotation(for node: SCNNode, direction: UISwipeGestureRecognizerDirection){
        switch direction{
        case .left:
            node.runAction(SCNAction.rotateBy(x: 0, y: -0.5, z: 0, duration: 0.1))
        case .right:
            node.runAction(SCNAction.rotateBy(x: 0, y: 0.5, z: 0, duration: 0.1))
        default:
            break
        }
        
    }
    
    func handlePinch(for node: SCNNode, scale: CGFloat){
        node.runAction(SCNAction.scale(by: scale, duration: 0.5))
    }
 
    func box() -> SCNNode{
        boxNode.eulerAngles = SCNVector3(90.toRadians, 0, 0)
        boxNode.geometry = SCNBox(width: boxSize.width, height: boxSize.height, length: boxSize.length, chamferRadius: boxSize.chamferRadius)
        boxNode.geometry?.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "box")
        boxNode.name = "box\(nodeNumber)"
        nodeNumber += 1
        return boxNode.clone()
    }
    
    func ship() -> SCNNode{
        shipNode.eulerAngles = SCNVector3Zero
        shipNode.scale = SCNVector3(0.05, 0.05, 0.05)
        shipNode.name = "ship\(nodeNumber)"
        nodeNumber += 1
        return shipNode.clone()
    }
    
    func car() -> SCNNode{
        carNode.eulerAngles = SCNVector3(90.toRadians, 0, 0)
        carNode.scale = SCNVector3(0.005, 0.005, 0.005)
        carNode.name = "car\(nodeNumber)"
        nodeNumber += 1
        return carNode.clone()
    }
    
    func aircraft() -> SCNNode{
        aircraftNode.eulerAngles = SCNVector3(0, 0, 0)
        aircraftNode.scale = SCNVector3(0.005, 0.005, 0.005)
        aircraftNode.name = "car\(nodeNumber)"
        nodeNumber += 1
        return aircraftNode.clone()
    }
    
    func plane(anchor: ARPlaneAnchor) -> SCNNode{
        let plane = SCNNode()
        plane.name = "plane\(nodeNumber)"
        plane.eulerAngles = SCNVector3(90.toRadians, 0, 0)
        plane.geometry = SCNPlane(width: CGFloat(anchor.extent.x), height: CGFloat(anchor.extent.z))
        plane.geometry?.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "square")
        plane.geometry?.firstMaterial?.isDoubleSided = true
        plane.position = SCNVector3(anchor.center.x, anchor.center.y, anchor.center.z)
        nodeNumber += 1
        return plane
    }
}

    // MARK: - ARSCNViewDelegate
extension ARViewController: ARSCNViewDelegate{
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        guard let newNode = selectedNode else {
            node.addChildNode(box())
            return
        }
        addGesture()
        newNode.position = SCNVector3(planeAnchor.center.x, planeAnchor.center.y, planeAnchor.center.z)
        node.addChildNode(newNode)
    }

//    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
//        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
//        removeNode(named: "box")
//
//        let boxNode = addBox(anchor: planeAnchor)
//        node.addChildNode(boxNode)
//    }
    
//    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
//        guard let name = node.name else { return }
//        print(name)
//        removeNode(named: name)
//    }
    
    var boxSize: (width: CGFloat, height: CGFloat, length: CGFloat, chamferRadius: CGFloat){
        let width: CGFloat = 0.1 * scale
        let heigth: CGFloat = 0.1 * scale
        let length: CGFloat = 0.1 * scale
        let chamferRadius: CGFloat = 0.0
        let boxSize = (width: width, height: heigth, length: length, chamferRadius: chamferRadius)
        return boxSize
    }
    
    func removeNode(named name: String){
        sceneView.scene.rootNode.enumerateChildNodes { (node, _) in
            if node.name == name{
                node.removeFromParentNode()
            }
        }
    }
}

extension ARViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.reuseId, for: indexPath) as! Cell
        let item = items[indexPath.item]
        if let image = UIImage(named: item){
            cell.config(with: image)
            cell.state = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.reuseId, for: indexPath) as! Cell
        guard let object = Objects(rawValue: indexPath.item) else { return }
        switch object{
        case .box:
            configNode(.box)
        case .car:
            configNode(.car)
        case .ship:
            configNode(.ship)
        case .aircraft:
            configNode(.aircraft)
        }
        cell.state = true
    }
    
    func configNode(_ nodeType: Objects){
        switch nodeType{
        case .box:
            selectedNode = box()
        case .car:
            selectedNode = car()
        case .ship:
            selectedNode = ship()
        case .aircraft:
            selectedNode = aircraft()
        }
    }
}
