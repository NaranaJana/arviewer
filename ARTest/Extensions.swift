//
//  Extensions.swift
//  ARTest
//
//  Created by test on 14.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import ARKit

public extension Float{
    public static var random: Float{
        return Float(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    public static func random(_ min: Float, _ max: Float) -> Float{
        return Float.random * (max - min) + min
    }
}

extension Int{
    var toRadians: Double{
        return Double(self) * .pi / 180
    }
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

extension UIView{
    static var nib: UINib! {
        return UINib(nibName: reuseId, bundle: nil)
    }
    
    static var reuseId: String! {
        return String(describing: self)
    }
    
    class func view<T: UIView>() -> T{
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension UIViewController{
    enum NavigationStyle: Int{
        case dark, light
    }
    
    private static var reuseId: String!{
        return String(describing: self)
    }
    
    private static func controller<T>(_ controller: T.Type) -> T{
        return UIStoryboard(name: reuseId, bundle: nil).instantiateViewController(withIdentifier: reuseId) as! T
    }
    
    static func instantiate() -> Self{
        return controller(self)
    }
    
    func configNavigationStyle(_ style: NavigationStyle, titleView: UIView? = nil){
        switch style{
        case .dark:
            navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "nav"), for: .default)
            navigationController?.navigationBar.barStyle = .blackOpaque
        case .light:
            navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.6509803922, green: 0.8745098039, blue: 0, alpha: 1)
            navigationController?.navigationBar.setBackgroundImage(UIImage(color: #colorLiteral(red: 0.6509803922, green: 0.8745098039, blue: 0, alpha: 1)), for: .default)
            navigationController?.navigationBar.barStyle = .default
        }
        if titleView != nil{
            navigationItem.titleView = titleView
        }
    }
    
    func takeScreenshot(_ view: ARSCNView) {
        //Create the UIImage
        
        let image = view.snapshot()
//        UIGraphicsBeginImageContext(view.frame.size)
//        view.layer.render(in: UIGraphicsGetCurrentContext()!)
//
//        let image = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
        //Save it to the camera roll
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
    }
    
}

extension UICollectionView{
    func dequeueReusableCell<T: UICollectionViewCell>(ofType type: T.Type, for indexPath: IndexPath) -> T{
        return dequeueReusableCell(withReuseIdentifier: T.reuseId, for: indexPath) as! T
    }
    func register<T: UICollectionViewCell>(cell: T.Type, bundle: Bundle? = nil){
        register(T.nib, forCellWithReuseIdentifier: T.reuseId)
    }
}

extension UITableView{
    func dequeueReusableCell<T: UITableViewCell>(ofType type: T.Type, for indexPath: IndexPath) -> T{
        return dequeueReusableCell(withIdentifier: T.reuseId, for: indexPath) as! T
    }
    func register<T: UITableViewCell>(cell: T.Type, bundle: Bundle? = nil){
        register(T.nib, forCellReuseIdentifier: T.reuseId)
    }
}

extension UICollectionViewCell{
    open override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.cornerRadius = 5.0
        contentView.layer.masksToBounds = true
    }
}

extension UIPageViewController {
    var isPagingEnabled: Bool {
        get {
            var isEnabled: Bool = true
            for view in view.subviews {
                if let subView = view as? UIScrollView {
                    isEnabled = subView.isScrollEnabled
                }
            }
            return isEnabled
        }
        set {
            for view in view.subviews {
                if let subView = view as? UIScrollView {
                    subView.isScrollEnabled = newValue
                }
            }
        }
    }
}
