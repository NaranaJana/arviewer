//
//  Models.swift
//  ARTest
//
//  Created by test on 18.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import SceneKit

struct MenuModel{
    var name: String?
    var internalFolders: [MenuModel]?
    var files: [FileModel]?
}

struct FileModel{
    var name: String
    var shownName: String
    var file: SCNNode?
}

struct FileToMove{
    var items: [SingleFile] = []
    static var instanceType = FileToMove()
}
struct SingleFile{
    var item: FileModel!
}
