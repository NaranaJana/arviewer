//
//  ViewController.swift
//  ARTest
//
//  Created by test on 11.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ARViewController: UIViewController{

    @IBOutlet var sceneView: ARSCNView!
    let configuration = ARWorldTrackingConfiguration()
    
    var scale: CGFloat = 1.0
    
    var selectedNode: SCNNode!
    
    var focalNode: FocalNode?
    var screenCenter: CGPoint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupSceneView()
        
        let logo: UIImage = #imageLiteral(resourceName: "logo_text")
        let logoView = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        logoView.contentMode = .scaleAspectFit
        logoView.image = logo
        configNavigationStyle(.dark, titleView: logoView)
    }
    
    func setupSceneView(){
        configuration.planeDetection = .horizontal
        
        if ARWorldTrackingConfiguration.isSupported{
            sceneView.session.run(configuration, options: [.removeExistingAnchors, .resetTracking])
        }
        
        sceneView.delegate = self
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
//        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
    }
    
    var footerView: UIView!
    
    func configUI(){
        
//        config footerView
        let yPos = view.bounds.height - 100
        let footerView = UIView(frame: CGRect(x: 0, y: yPos, width: view.bounds.width, height: 80))
        footerView.backgroundColor = .clear
        
//        config shareButton and add it to footer
        let shareButton = UIButton(frame: CGRect(x: 10, y: 10, width: 60, height: 60))
        shareButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        shareButton.layer.cornerRadius = shareButton.frame.height / 2
        shareButton.setImage(#imageLiteral(resourceName: "ic_share"), for: .normal)
        shareButton.addTarget(self, action: #selector(shareAction(_:)), for: .touchUpInside)
        footerView.addSubview(shareButton)
        
//        config pasteButton and add it to footer
        let pasteButton = UIButton(frame: CGRect(x: (footerView.frame.width / 2) - 30, y: 10, width: 60, height: 60))
        pasteButton.backgroundColor = #colorLiteral(red: 0.6509803922, green: 0.8745098039, blue: 0, alpha: 1)
        pasteButton.layer.cornerRadius = pasteButton.frame.height / 2
        pasteButton.setImage(#imageLiteral(resourceName: "ic_paste_center"), for: .normal)
        pasteButton.addTarget(self, action: #selector(handleTap), for: .touchUpInside)
        footerView.addSubview(pasteButton)
        
//        config screenshotButton and add it to footer
        let saveButton = UIButton(frame: CGRect(x: view.bounds.width - 70, y: 10, width: 60, height: 60))
        saveButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
        saveButton.setImage(#imageLiteral(resourceName: "ic_save"), for: .normal)
        saveButton.addTarget(self, action: #selector(saveAction(_:)), for: .touchUpInside)
        footerView.addSubview(saveButton)
        
//        assign local footer to global footer to access it thro all class
        self.footerView = footerView
        view.addSubview(footerView)
        
//        config center of screen
        screenCenter = view.center
    }
    
    @objc func saveAction(_ sender: UIButton){
        takeScreenshot(sceneView)
        
        let stateView = configLabel(with: "Можешь посмотреть свой скриншот в галерее!")
        view.addSubview(stateView)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            stateView.removeFromSuperview()
        }
    }
    
    @objc func shareAction(_ sender: UIButton){
        let message = "Ля, чо могу"
        let image = sceneView.snapshot()
        
        let objectsToShare = [message, image] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
        self.present(activityVC, animated: true, completion: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    func addGesture(){
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(didRecognize(_:)))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didRecognize(_:)))
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(didRecognize(_:)))
        let rotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(didRecognize(_:)))
        
        DispatchQueue.main.async {[weak self] in
            guard let strongSelf = self else { return }
            tapGesture.cancelsTouchesInView = false
            pinchGesture.cancelsTouchesInView = false
            panGesture.cancelsTouchesInView = false
            rotationGesture.cancelsTouchesInView = false
            strongSelf.sceneView.addGestureRecognizer(pinchGesture)
            strongSelf.sceneView.addGestureRecognizer(tapGesture)
            strongSelf.sceneView.addGestureRecognizer(panGesture)
            strongSelf.sceneView.addGestureRecognizer(rotationGesture)
        }
    }
    
    @objc func didRecognize(_ sender: UIGestureRecognizer){
        if let rotation = sender as? UIRotationGestureRecognizer{
            if rotation.state == .changed{
                let node = getNode(from: rotation)
                handleRotation(for: node, rotation: rotation.rotation)
            }
        }else if let pinch = sender as? UIPinchGestureRecognizer{
            if pinch.state == .changed{
                let node = getNode(from: pinch)
                scale = pinch.scale
                handlePinch(for: node, scale: scale)
                pinch.scale = 1.0
            }
        }else if let tap = sender as? UITapGestureRecognizer{
            let node = getNode(from: tap)
            if let name = node.name{
                removeNode(named: name)
            }
        }else if let pan = sender as? UIPanGestureRecognizer{
            if pan.state == .changed{
                let node = getNode(from: pan)
                handlePan(for: node, location: pan.location(in: sceneView))
            }
        }
    }
    
    func reconfigUI(_ hidden: Bool){
        UIView.animate(withDuration: 0.3) {[weak self] in
            guard let strongSelf = self else { return }
            switch hidden{
            case true:
                strongSelf.footerView.frame.origin.y += 100
            case false:
                strongSelf.footerView.frame.origin.y -= 100
            }
        }
        navigationController?.setNavigationBarHidden(hidden, animated: true)
    }
    
    @IBAction func infoAction(_ sender: UIBarButtonItem) {
        let hint = PageViewController.instantiate()
        hint.hintDelegate = self
        hint.modalPresentationStyle = .overCurrentContext
        
        reconfigUI(true)
        
        present(hint, animated: true, completion: nil)
    }
    
    @IBAction func addNewAction(_ sender: UIBarButtonItem) {
        let menu = MenuViewController.instantiate()
        menu.delegate = self
        navigationController?.pushViewController(menu, animated: true)
    }
    
    func getNode(from gesture: UIGestureRecognizer) -> SCNNode{
        let location = gesture.location(in: sceneView)
        let hits = sceneView.hitTest(location, options: nil)
        guard !hits.isEmpty else { return SCNNode() }
        guard let node = hits.first?.node else { return SCNNode() }
        return node
    }
    
    @objc func handleTap(){
        guard focalNode != nil, selectedNode != nil else { return }
        
        let results = sceneView.hitTest(screenCenter, types: .existingPlane)
        
        guard let transform = results.first?.worldTransform else { return }
        
        let position = float3(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
        let node = selectedNode.flattenedClone()
        node.simdPosition = position
        
        sceneView.scene.rootNode.addChildNode(node)
    }
    
    func handleRotation(for node: SCNNode, rotation: CGFloat){
        var originalRotation: SCNVector3 = SCNVector3(0, 0, 0)
        
        originalRotation.y -= Float(rotation)
        node.eulerAngles = originalRotation
    }
    
    func handlePinch(for node: SCNNode, scale: CGFloat){
        node.runAction(SCNAction.scale(by: scale, duration: 0.5))
    }
    
    func handlePan(for node: SCNNode, location: CGPoint){
        guard let result = sceneView.hitTest(location, types: .existingPlane).first else { return }
        
        let transform = result.worldTransform
        let newPosition = float3(x: transform.columns.3.x, y: transform.columns.3.y, z: transform.columns.3.z)
        node.simdPosition = newPosition
    }
    
    func configLabel(with text: String) -> UIView{
        let stateView = UIView(frame: CGRect(x: view.bounds.width / 2 - 100, y: view.bounds.height / 2 - 100, width: 200, height: 200))
        stateView.layer.cornerRadius = 10.0
        stateView.layer.masksToBounds = true
        stateView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: stateView.frame.width, height: stateView.frame.height))
        label.text = text
        label.font = UIFont.boldSystemFont(ofSize: 19.0)
        label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        label.textAlignment = .center
        label.numberOfLines = 0
        stateView.addSubview(label)
        
        return stateView
    }
}

    // MARK: - ARSCNViewDelegate
extension ARViewController: ARSCNViewDelegate{
//    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
//        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
//        guard let newNode = selectedNode else { return }
//        addGesture()
//        newNode.position = SCNVector3(planeAnchor.center.x, planeAnchor.center.y, planeAnchor.center.z)
//        newNode.scale = SCNVector3(0.05, 0.05, 0.05)
////        newNode.eulerAngles = SCNVector3Zero
//        node.addChildNode(newNode)
//    }

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard focalNode == nil, selectedNode != nil else { return }
        let node = FocalNode()
        
//        node.addChildNode(selectedNode)
        
        focalNode = node
        sceneView.scene.rootNode.addChildNode(node)
        
        addGesture()
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        guard let focalNode = focalNode else { return }
        
        let hit = sceneView.hitTest(screenCenter, types: .existingPlane)
        
        guard let positionColumn = hit.first?.worldTransform.columns.3 else { return }
        focalNode.position = SCNVector3(positionColumn.x, positionColumn.y, positionColumn.z)
    }
    
    func removeNode(named name: String){
        sceneView.scene.rootNode.enumerateChildNodes { (node, _) in
            if node.name == name{
                node.removeFromParentNode()
            }
        }
    }
}

//MARK: - MenuViewControllerDelegate
extension ARViewController: MenuViewControllerDelegate{
    func menuViewController(_ menuViewController: MenuViewController, didSelectObject object: FileModel?) {
        selectedNode = object?.file
    }
}

//MARK: - PageViewControllerDelegate
extension ARViewController: PageViewControllerDelegate{
    func viewControllerDisappear(_ viewController: PageViewController) {
        reconfigUI(false)
    }
}
