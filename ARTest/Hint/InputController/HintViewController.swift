//
//  HintViewController.swift
//  ARTest
//
//  Created by test on 19.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

enum HintSections: Int{
    case skip, title, separatorOne, description, image, separatorTwo, nextButton, count
}

protocol HintViewControllerDelegate: class{
    func hintViewController(_ hintViewController: HintViewController, shoudDisplayNext hint: Bool)
}

class HintViewController: UITableViewController{
    
    private var hintTitle = ""
    private var hintMessage = ""
    private var hintImage = UIImage()
    
    weak var delegate: HintViewControllerDelegate?
    var currentIndex = 0
    var totalIndexed: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(cell: TitleCell.self)
        tableView.register(cell: DescriptionCell.self)
        tableView.register(cell: ImageCell.self)
        tableView.register(cell: DoneButtonCell.self)
        tableView.register(cell: FakeCell.self)
        tableView.register(cell: SkipCell.self)
        tableView.separatorColor = .clear
    }
    
    func config(title: String, message: String, image: UIImage){
        hintTitle = title
        hintMessage = message
        hintImage = image
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return HintSections.count.rawValue
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = HintSections(rawValue: section) else { return 0 }
        switch section{
        case .count: return 0
        case .description: return 1
        case .image: return 1
        case .nextButton: return 1
        case .title: return 1
        case .skip: return 1
        default: return 1
        }
    }
    
    var isNext = true
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = HintSections(rawValue: indexPath.section) else { return UITableViewCell() }
        switch section{
        case .count:
            return UITableViewCell()
        case .description:
            let cell = tableView.dequeueReusableCell(ofType: DescriptionCell.self, for: indexPath)
            cell.config(desc: hintMessage)
            return cell
        case .image:
            let cell = tableView.dequeueReusableCell(ofType: ImageCell.self, for: indexPath)
            cell.config(image: hintImage)
            return cell
        case .nextButton:
            let cell = tableView.dequeueReusableCell(ofType: DoneButtonCell.self, for: indexPath)
            if currentIndex != totalIndexed{
                cell.hintNextButton.setTitle("Дальше", for: .normal)
            }else{
                cell.hintNextButton.setTitle("Закрыть", for: .normal)
                isNext = false
            }
            cell.delegate = self
            return cell
        case .title:
            let cell = tableView.dequeueReusableCell(ofType: TitleCell.self, for: indexPath)
            cell.config(title: hintTitle)
            return cell
        case .skip:
            let cell = tableView.dequeueReusableCell(ofType: SkipCell.self, for: indexPath)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(ofType: FakeCell.self, for: indexPath)
            return cell
        }
    }
}

extension HintViewController: DoneButtonCellDelegate{
    func tappedCell(_ cell: DoneButtonCell) {
        delegate?.hintViewController(self, shoudDisplayNext: isNext)
    }
}
