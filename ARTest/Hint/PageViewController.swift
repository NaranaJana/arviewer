//
//  HintViewController.swift
//  ARTest
//
//  Created by test on 19.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

enum CurrentHint: Int{
    case initial
    case swipe
    case scale
    case remove
    
    var title: String{
        switch self{
        case .initial: return ""
        case .scale: return "Изменение масштаба объекта"
        case .swipe: return "Поворот объекта"
        case .remove: return "Удаление объекта"
        }
    }
    
    var desc: String{
        switch self{
        case .initial: return "Для начала наведи камеру на то место(плоскость), где желаешь разместить объект"
        case .scale: return "Ты можешь изменить масштаб 3D объекта с помощью двух пальцев"
        case .swipe: return "Чтобы повернуть объект проведи по нему вправо или влево"
        case .remove: return "Чтобы удалить объект, просто нажми на него"
        }
    }
    
    var image: UIImage{
        switch self{
        case .initial: return #imageLiteral(resourceName: "picture_3d")
        case .swipe: return #imageLiteral(resourceName: "ic_touch_2")
        case .scale: return #imageLiteral(resourceName: "ic_touch_3")
        case .remove: return #imageLiteral(resourceName: "ic_touch_1")
        }
    }
}

protocol PageViewControllerDelegate: class{
    func viewControllerDisappear(_ viewController: PageViewController)
}

class PageViewController: UIPageViewController{
    
    private(set) lazy var orderedViewControllers = [HintViewController.instantiate(), HintViewController.instantiate(), HintViewController.instantiate(), HintViewController.instantiate()]

    var skipButton: UIButton!
    
    weak var hintDelegate: PageViewControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configNavigationStyle(.dark)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        
        isPagingEnabled = false
        
        if let firstVC = orderedViewControllers.first{
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        
        for index in 0..<orderedViewControllers.count{
            guard let hint = CurrentHint(rawValue: index) else { return }
            orderedViewControllers[index].config(title: hint.title, message: hint.desc, image: hint.image)
            orderedViewControllers[index].delegate = self
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hintDelegate?.viewControllerDisappear(self)
    }
    
    var nextIndex = 0
}

extension PageViewController: UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? HintViewController else { return nil  }
        guard let index = orderedViewControllers.index(of: vc) else { return nil }
        let previous = index - 1
        guard previous >= 0, orderedViewControllers.count > previous else { return nil }
        return orderedViewControllers[previous]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? HintViewController else { return nil }
        guard let index = orderedViewControllers.index(of: vc) else { return nil }
        let next = index + 1
        let count = orderedViewControllers.count
        guard count != next, count > next else { return nil }
        return orderedViewControllers[next]
    }
}

extension PageViewController: HintViewControllerDelegate{
    func hintViewController(_ hintViewController: HintViewController, shoudDisplayNext hint: Bool) {
        if hint{
            let array = orderedViewControllers
            nextIndex += 1
            guard array.count > nextIndex else { return }
            array[nextIndex].currentIndex = nextIndex + 1
            array[nextIndex].totalIndexed = array.count
            setViewControllers([array[nextIndex]], direction: .forward, animated: true, completion: nil)
        }else{
         dismiss(animated: true, completion: nil)
        }
    }
}
