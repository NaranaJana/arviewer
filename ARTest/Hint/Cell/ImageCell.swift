//
//  ImageCell.swift
//  ARTest
//
//  Created by test on 19.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell{
    @IBOutlet weak var hintImageView: UIImageView!
    
    func config(image: UIImage){
        hintImageView.image = image
    }
}
