//
//  DoneButtonCell.swift
//  ARTest
//
//  Created by test on 19.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

protocol DoneButtonCellDelegate: class{
    func tappedCell(_ cell: DoneButtonCell)
}

class DoneButtonCell: UITableViewCell{
    @IBOutlet weak var hintNextButton: UIButton!
    
    weak var delegate: DoneButtonCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        hintNextButton.layer.cornerRadius = 5.0
        hintNextButton.layer.masksToBounds = true
    }
    @IBAction func nextButtonAction(_ sender: UIButton) {
        delegate?.tappedCell(self)
    }
}
