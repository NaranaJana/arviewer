//
//  DescriptionCell.swift
//  ARTest
//
//  Created by test on 19.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class DescriptionCell: UITableViewCell{
    @IBOutlet weak var hintDescLabel: UILabel!
    
    func config(desc: String){
        hintDescLabel.text = desc
    }
}
