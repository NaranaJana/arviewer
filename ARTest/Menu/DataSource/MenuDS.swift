//
//  MenuDS.swift
//  ARTest
//
//  Created by test on 20.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import SceneKit

protocol DataSource{
    associatedtype SomeType
    
    func getAllEntities(completion:@escaping (_ data:SomeType?, _ error: Error?) -> Void)
}

class MenuDS: DataSource{
    
    var ship = ARConfigurator.configNode(.daeBased, named: "veliero")
    var car = ARConfigurator.configNode(.daeBased, named: "car")
    var aircraft = ARConfigurator.configNode(.sceneBased, named: "aircraft")
    var box = ARConfigurator.configNode(.standard, isStandard: .box, texture: #imageLiteral(resourceName: "box"))
    var pyramid = ARConfigurator.configNode(.standard, isStandard: .pyramid, texture: #imageLiteral(resourceName: "pyramid"))
    let chair = ARConfigurator.configNode(.daeBased, named: "chair")
    let cat = ARConfigurator.configNode(.daeBased, named: "cat")
    let torus = ARConfigurator.configNode(.standard, isStandard: .torus, texture: #imageLiteral(resourceName: "box"))
    
    func getAllEntities(completion: @escaping (MenuModel?, Error?) -> Void) {
        var menu: MenuModel? = nil
        let files = [ship, car, aircraft, box, pyramid, chair, cat, torus]
        
        let folderNames = ["Transport", "Standard", "Lanscapes"]
        var folders = [MenuModel]()
        
        var menuFiles = [FileModel]()
        
        for file in files{
            let menuFile = FileModel.init(name: file!.name!, shownName: file!.name!, file: file)
            menuFiles.append(menuFile)
        }
        
        for folder in folderNames{
            let internalFolder = MenuModel.init(name: folder, internalFolders: nil, files: menuFiles)
            folders.append(internalFolder)
        }
        
        menu = MenuModel.init(name: nil, internalFolders: folders, files: menuFiles)
        completion(menu, nil)
    }
}
