//
//  MenuViewController.swift
//  ARTest
//
//  Created by test on 18.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import SceneKit

enum Sections: Int{
    case folders, files, count
}

enum State: Int{
    case hidden, visible
}

protocol MenuViewControllerDelegate: class{
    func menuViewController(_ menuViewController: MenuViewController, didSelectObject object: FileModel?)
}

class MenuViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var collectionView: UICollectionView!
    
    var subMenuView: SubMenuView!
    
    weak var delegate: MenuViewControllerDelegate!
    
    let ds = MenuDS()
    var menu: MenuModel?
    
    @IBOutlet var searchBar: UISearchBar!
    var fakeTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configNavigationStyle(.light)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(cell: MenuFolderCell.self)
        collectionView.register(cell: MenuItemsCell.self)
        configUI()
        
        if menu == nil{
            ds.getAllEntities {[weak self] (menu, error) in
                guard let strongSelf = self, let menu = menu else { return }
                strongSelf.menu = menu
                strongSelf.collectionView.reloadData()
            }
            let logo: UIImage = #imageLiteral(resourceName: "logo_text")
            let logoView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
            logoView.contentMode = .scaleAspectFit
            logoView.image = logo
            navigationItem.titleView = logoView
        }else{
            title = menu!.name
            if FileToMove.instanceType.items.count != 0{
                for item in FileToMove.instanceType.items{
                    menu?.files?.append(item.item!)
                }
            }
        }
    }
    
    func configUI(){
        subMenuView = .view()
        subMenuView.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: 230)
        subMenuView.editButton.addTarget(self, action: #selector(editAction), for: .touchUpInside)
        subMenuView.deleteButton.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        subMenuView.moveButton.addTarget(self, action: #selector(moveAction), for: .touchUpInside)
        view.addSubview(subMenuView)
        
        fakeTextField = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        fakeTextField.delegate = self
        fakeTextField.inputAccessoryView = searchBar
        view.addSubview(fakeTextField)
    }
    
    @objc func editAction(){
        subMenuState(state)
        performAction(.edit, for: subMenuView.cell)
    }
    
    @objc func deleteAction(){
        subMenuState(state)
        performAction(.delete, for: subMenuView.cell)
    }
    
    @objc func moveAction(){
        subMenuState(state)
        performAction(.move, for: subMenuView.cell)
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func infoAction(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        fakeTextField.becomeFirstResponder()
        searchBar.delegate = self
        searchBar.becomeFirstResponder()
    }
    
    @IBAction func addAction(_ sender: UIBarButtonItem) {
    }
    
    var state: State = .hidden
    
    func subMenuState(_ state: State, title: String? = nil, cell: UICollectionViewCell? = nil){
        UIView.animate(withDuration: 0.2) {[weak self] in
            guard let strongSelf = self else { return }
            switch state{
            case .hidden:
                strongSelf.subMenuView.frame.origin.y -= strongSelf.subMenuView.frame.height - 10
                strongSelf.state = .visible
            case .visible:
                strongSelf.subMenuView.frame.origin.y += strongSelf.subMenuView.frame.height - 10
                strongSelf.state = .hidden
            }
        }
        if let title = title{
            subMenuView.folderNameLabel.text = title
        }
        if let cell = cell{
            subMenuView.cell = cell
        }
    }
    
    func performAction(_ action: SelectedAction, for cell: UICollectionViewCell){
        guard let indexPath = collectionView.indexPath(for: cell), let section = Sections(rawValue: indexPath.section) else { return }
        switch (action, section){
        case (.delete, .files):
            collectionView.performBatchUpdates({[weak self] in
                guard let strongSelf = self else { return }
                strongSelf.menu?.files?.remove(at: indexPath.item)
                collectionView.deleteItems(at: [IndexPath(item: indexPath.item, section: indexPath.section)])
                }, completion: nil)
        case (.delete, .folders):
            collectionView.performBatchUpdates({[weak self] in
                guard let strongSelf = self else { return }
                strongSelf.menu?.internalFolders?.remove(at: indexPath.item)
                collectionView.deleteItems(at: [IndexPath(item: indexPath.item, section: indexPath.section)])
                }, completion: nil)
        case (.move, .files):
            collectionView.performBatchUpdates({[weak self] in
                guard let strongSelf = self else { return }
                let item = SingleFile.init(item: strongSelf.menu?.files![indexPath.item])
                FileToMove.instanceType.items.append(item)
                strongSelf.menu?.files?.remove(at: indexPath.item)
                collectionView.deleteItems(at: [IndexPath(item: indexPath.item, section: indexPath.section)])
            }, completion: nil)
        case (.edit, .files):
            guard let cell = cell as? MenuItemsCell else { return }
            cell.itemNameTextField.isEnabled = true
            cell.itemNameTextField.delegate = self
            cell.itemNameTextField.becomeFirstResponder()
        case (.edit, .folders):
            guard let cell = cell as? MenuFolderCell else { return }
            cell.folderNameTextField.isEnabled = true
            cell.folderNameTextField.delegate = self
            cell.folderNameTextField.becomeFirstResponder()
        default:
            break
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Sections.count.rawValue
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sections = Sections(rawValue: section), let menu = menu else { return 0 }
        switch sections{
        case .folders:
            guard menu.internalFolders != nil else { return 0 }
            return menu.internalFolders!.count
        case .files:
            guard menu.files != nil else { return 0 }
            return menu.files!.count
        case .count:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let section = Sections(rawValue: indexPath.section),
            let menu = menu
            else { return UICollectionViewCell() }
        switch section{
        case .folders:
            let cell = collectionView.dequeueReusableCell(ofType: MenuFolderCell.self, for: indexPath)
            let menuItem = menu.internalFolders![indexPath.item]
            cell.config(item: menuItem)
            cell.delegate = self
            return cell
        case .files:
            let cell = collectionView.dequeueReusableCell(ofType: MenuItemsCell.self, for: indexPath)
            let file = menu.files![indexPath.item]
            cell.config(item: file)
            cell.delegate = self
            return cell
        case .count:
            return UICollectionViewCell()
        }
    }
    
    var available = false
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let section = Sections(rawValue: indexPath.section), let menu = menu else { return }
        switch section{
        case .files:
            delegate.menuViewController(self, didSelectObject: menu.files![indexPath.item])
            navigationController?.popViewController(animated: true)
        case .folders:
            let subMenu = MenuViewController.instantiate()
            subMenu.delegate = self
            subMenu.menu = menu.internalFolders![indexPath.item]
            navigationController?.pushViewController(subMenu, animated: true)
        case .count:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind{
        case UICollectionElementKindSectionHeader:
            guard let section = Sections(rawValue: indexPath.section) else { return UICollectionReusableView() }
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: SectionHeaderCollectionReusableView.reuseId, for: indexPath) as! SectionHeaderCollectionReusableView
            switch section{
            case .count:
                break
            case .files:
                headerView.headerLabel.text = "Файлы"
            case .folders:
                headerView.headerLabel.text = "Папки"
            }
            return headerView
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        guard let section = Sections(rawValue: indexPath.section) else { return CGSize(width: 0, height: 0) }
        switch section{
        case .count:
            return CGSize(width: 0, height: 0)
        case .files:
            let width = collectionView.frame.width / 2 - 11
            let height = collectionView.frame.height / 2.5 - 11
            return CGSize(width: width, height: height)
        case .folders:
            let width = collectionView.frame.width - 10
            let height: CGFloat = 50.0
            return CGSize(width: width, height: height)
        }
    }
}

extension MenuViewController: MenuItemsCellDelegate{
    func buttonPressed(cell: MenuItemsCell) {
        subMenuState(state, title: cell.itemNameTextField.text, cell: cell)
    }
}

extension MenuViewController: MenuFolderCellDelegate{
    func buttonPressed(cell: MenuFolderCell) {
        subMenuState(state, title: cell.folderNameTextField.text, cell: cell)
    }
}

extension MenuViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.isEnabled = false
        if let cell = subMenuView.cell{
            if let indexPath = collectionView.indexPath(for: cell){
                guard let cell = collectionView.cellForItem(at: indexPath) else { return true }
                if let folderCell = cell as? MenuFolderCell{
                    menu!.internalFolders![collectionView.indexPath(for: folderCell)!.item].name = folderCell.folderNameTextField.text
                }
            }
        }
        view.endEditing(true)
        return true
    }
}

extension MenuViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let _ = textFieldShouldReturn(fakeTextField)
        view.endEditing(true)
        
        let newFiles = menu?.files?.filter{
            return $0.shownName.lowercased() == searchBar.text?.lowercased()
        }
        
        menu?.files = newFiles
        collectionView.reloadData()
    }
}

extension MenuViewController: MenuViewControllerDelegate{
    func menuViewController(_ menuViewController: MenuViewController, didSelectObject object: FileModel?) {
        delegate.menuViewController(menuViewController, didSelectObject: object)
        navigationController?.popViewController(animated: true)
    }
}
