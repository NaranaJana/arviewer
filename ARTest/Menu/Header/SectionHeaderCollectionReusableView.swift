//
//  SectionHeaderCollectionReusableView.swift
//  ARTest
//
//  Created by test on 19.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class SectionHeaderCollectionReusableView: UICollectionReusableView{
    @IBOutlet weak var headerLabel: UILabel!
}
