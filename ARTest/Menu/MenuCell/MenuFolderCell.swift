//
//  MenuFolderCell.swift
//  ARTest
//
//  Created by test on 18.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

protocol MenuFolderCellDelegate: class {
    func buttonPressed(cell: MenuFolderCell)
}

class MenuFolderCell: UICollectionViewCell{
    @IBOutlet weak var folderNameTextField: UITextField!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var cellWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var cellBGView: UIView!
    weak var delegate: MenuFolderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellWidthConstraint.constant = UIScreen.main.bounds.width
        folderNameTextField.isEnabled = false
        folderNameTextField.text = "Folder"
    }
    
    func config(item: MenuModel){
        folderNameTextField.text = item.name
    }
    
    @IBAction func moreAction(_ sender: UIButton) {
        delegate?.buttonPressed(cell: self)
    }
}
