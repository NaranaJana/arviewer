//
//  MenuCell.swift
//  ARTest
//
//  Created by test on 18.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import SceneKit

protocol MenuItemsCellDelegate: class{
    func buttonPressed(cell: MenuItemsCell)
}

class MenuItemsCell: UICollectionViewCell{
    @IBOutlet weak var itemNameTextField: UITextField!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var sceneView: SCNView!
    weak var delegate: MenuItemsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        itemNameTextField.isEnabled = false
        itemNameTextField.text = "Item name"
    }
    
    func config(item: FileModel){
        itemNameTextField.text = item.shownName
        let scene = SCNScene()
        scene.background.contents = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
        let file = item.file
        scene.rootNode.addChildNode(file!)
        sceneView.scene = scene
    }
    
    @IBAction func buttonAction(_ sender: UIButton){
        delegate?.buttonPressed(cell: self)
    }
}
