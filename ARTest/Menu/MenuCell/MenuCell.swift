//
//  MenuCell.swift
//  ARTest
//
//  Created by test on 18.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

protocol MenuCellDelegate: class{
    func buttonPressed(cell: MenuItemsCell)
}

class MenuItemsCell: UICollectionViewCell{
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    weak var delegate: MenuCellDelegate?
    
    static var nib: UINib! {
        return UINib(nibName: reuseId, bundle: nil)
    }
    
    static var reuseId: String! {
        return String(describing: self)
    }
    
    func config(_ name: String){
        itemNameLabel.text = name
    }
}
