//
//  SubMenuView.swift
//  ARTest
//
//  Created by test on 18.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

enum SelectedAction: Int{
    case edit, move, delete
}

class SubMenuView: UIView{
    @IBOutlet weak var folderNameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var moveButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    var cell: UICollectionViewCell!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 10.0
        layer.masksToBounds = true
    }
}
