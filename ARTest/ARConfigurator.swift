//
//  ARConfigurator.swift
//  ARTest
//
//  Created by test on 18.09.2018.
//  Copyright © 2018 test. All rights reserved.
//

import SceneKit

enum StandardTypes: Int{
    case box, capsule, cone, cylinder, plane, pyramid, sphere, torus, tube
    
    var title: String{
        switch self{
        case .box: return "Box"
        case .capsule: return "Capsule"
        case .cone: return "Cone"
        case .cylinder: return "Cylinder"
        case .plane: return "Plane"
        case .pyramid: return "Pyramid"
        case .sphere: return "Sphere"
        case .torus: return "Torus"
        case .tube: return "Tube"
        }
    }
}

enum NodeTypes: Int{
    case sceneBased, daeBased, standard
}

class ARConfigurator{
    class func configNode(_ type: NodeTypes?, isStandard standardType: StandardTypes? = nil, texture: UIImage? = nil, named name: String? = nil) -> SCNNode?{
        guard let type = type else { return nil }
        var node = SCNNode()
        switch type{
        case .daeBased:
            if name != nil{
                let route = "art.scnassets/\(name!)/\(name!).dae"
                let scene = SCNScene(named: route)!
                node = scene.rootNode.childNode(withName: name!, recursively: true)!
                node.name = name
            }
        case .sceneBased:
            if name != nil{
                let route = "art.scnassets/\(name!)/\(name!).scn"
                let scene = SCNScene(named: route)!
                node = scene.rootNode.childNode(withName: name!, recursively: true)!
                node.name = name
            }
        case .standard:
            let config = configStandard(standardType)
            node.geometry = config?.geometry
            node.name = config?.name
            if texture != nil {
               node.geometry?.firstMaterial?.diffuse.contents = texture!
            }
        }
        return node.clone()
    }
    
    class private func configStandard(_ type: StandardTypes?) -> (name: String?, geometry: SCNGeometry?)?{
        guard let type = type else { return nil }
        var geo = SCNGeometry()
        var geometry: (name: String?, geometry: SCNGeometry?) = (name: nil, geometry: nil)
        switch type{
        case .box:
            geo = SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0)
        case .capsule:
            geo = SCNCapsule(capRadius: 0.1, height: 0.1)
        case .cone:
            geo = SCNCone(topRadius: 0, bottomRadius: 0.1, height: 0.1)
        case .cylinder:
            geo = SCNCylinder(radius: 0.1, height: 0.1)
        case .plane:
            geo = SCNPlane(width: 0.1, height: 0.1)
        case .pyramid:
            geo = SCNPyramid(width: 0.1, height: 0.1, length: 0.1)
        case .sphere:
            geo = SCNSphere(radius: 0.1)
        case .torus:
            geo = SCNTorus(ringRadius: 0.1, pipeRadius: 0.05)
        case .tube:
            geo = SCNTube(innerRadius: 0.09, outerRadius: 0.1, height: 0.1)
        }
        geometry.name = type.title
        geometry.geometry = geo
        return geometry
    }
}
